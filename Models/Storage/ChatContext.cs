using System;
using Microsoft.EntityFrameworkCore;

namespace ChatServer.Models.Storage
{
    public class ChatContext : DbContext
    {
        public DbSet<User> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseInMemoryDatabase("chat");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>(entity => entity.HasIndex(e => e.Nickname).IsUnique());
        }
    }
}