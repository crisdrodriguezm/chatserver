using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using ChatServer.Services;

namespace ChatServer.Models
{
    public class ChatModel
    {
        private Dictionary<int, TcpClient> _clients = new Dictionary<int, TcpClient>();
        private Models.Storage.ChatContext _context;
        private MessageModel _messageModel;
        private JsonService _jsonService;

        public ChatModel()
        {
            _context = new Models.Storage.ChatContext();
            _messageModel = new MessageModel();
            _jsonService = new JsonService();
        }

        public void Start()
        {
            var listener = new TcpListener(IPAddress.Any, 8888);
            listener.Start();
            while (true)
            {
                var client = listener.AcceptTcpClient();
                new Thread(async () => await ListenClient(client)).Start();
            }
        }

        public async Task ListenClient(TcpClient client)
        {
            try
            {
                var reader = new StreamReader(client.GetStream());

                while (true)
                {
                    var line = reader.ReadLine();
                    if (!_clients.ContainsValue(client))
                    {
                        User publicUser;
                        if (line.Contains(":register:"))
                        {
                            var user = await _jsonService.GetDeserializedResponse<Models.Storage.User>(line.Replace(":register:", string.Empty));
                            if (!user.Password.Equals(user.PasswordConfirmation))
                            {
                                _messageModel.Send(client, "El password y la confirmación no coinciden");
                                continue;
                            }
                            else if (_context.Users.Any(u => u.Nickname.Equals(user.Nickname)))
                            {
                                _messageModel.Send(client, "El usuario ya existe, por favor pruebe iniciando sesión");
                                continue;
                            }
                            _context.Users.Add(user);
                            _context.SaveChanges();
                            publicUser = new User()
                            {
                                Id = user.Id,
                                Name = user.Nickname,
                                Nickname = user.Name
                            };
                            _clients.Add(user.Id, client);
                            _messageModel.Send(client, true);
                        }
                        else if (line.Contains(":login:"))
                        {
                            var user = await _jsonService.GetDeserializedResponse<Models.Storage.User>(line.Replace(":login:", string.Empty));
                            if (!_context.Users.Any(u => u.Nickname.Equals(user.Nickname) && u.Password.Equals(user.Password)))
                            {
                                _messageModel.Send(client, "El usuario y contraseña no corresponden con nuestros registros");
                                continue;
                            }
                            var storedUser = _context.Users.FirstOrDefault(u => u.Nickname.Equals(user.Nickname));
                            _clients.Add(storedUser.Id, client);
                            publicUser = new User()
                            {
                                Id = storedUser.Id,
                                Name = storedUser.Nickname,
                                Nickname = storedUser.Name
                            };
                            _messageModel.Send(client, true);
                        }
                        else
                        {
                            _messageModel.Send(client, "No has iniciado sesión");
                            continue;
                        }

                        _messageModel.Broadcast(_clients.Values.Where(c => c != client), $"{publicUser.Nickname} se ha unido a la sala");
                        continue;
                    }

                    _messageModel.Broadcast(_clients.Values.Where(c => c != client), $"{_context.Users.Find(_clients.FirstOrDefault(kvp => kvp.Value.Equals(client)).Key).Nickname}: {line}");
                }
            }
            catch (Exception e)
            {
                var cli = _clients.FirstOrDefault(c => c.Value.Equals(client));
                _messageModel.Broadcast(_clients.Values.Where(c => c != client), $"{_context.Users.Find(cli.Key).Nickname} abandono la sala");
                if (!cli.Equals(default(KeyValuePair<string, TcpClient>)))
                    _clients.Remove(cli.Key);
                Console.WriteLine($"Error: {e.Message}");
            }
        }
    }
}