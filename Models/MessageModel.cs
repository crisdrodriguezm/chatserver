using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;

namespace ChatServer.Models
{
    public class MessageModel
    {
        public void Send(TcpClient client, object message)
        {
            var writer = new StreamWriter(client.GetStream());
            writer.WriteLine(message);
            writer.Flush();
        }

        public void Broadcast(IEnumerable<TcpClient> clients, string message)
        {
            foreach (var client in clients)
                Send(client, message);
        }
    }
}